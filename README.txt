# README #

Lab Final Files:

Lab 1, Seth - Lab 1 main.c located in Lab_01/Seth
Lab 1, Noah - Lab 1 main.c located in Lab_01/Noah
Lab 2 - Peripherals (pwm.h, pwm.c, motor.h, motor.c) located in periph_n_blount, lab 2 main.c located in Lab_02/Noah_02 
Lab 3 - Peripherals (motor.c, motor.h, quadEnc.c, quadEnc.h) located in periph_n_blount, lab 3 main located in Lab_03, demonstration in Lab_03/Checkoff.zip
Lab 4 - Peripherals (bump.h, bump.c) located in  Lab_02/Seth02, lab 4 main.c (called lab2.c) located in Lab_02/Seth01, demonstration in Lab_04/Checkoff.zip
Lab 5 - Peripherals (ir.h, ir.c) located in periph_n_blount, lab 5 main.c located in Lab_05, demonstration in Lab_05/Checkoff.zip
Lab 6 - Pi File (piUART.py) located in pi_files, lab 6 main located in Lab_06
Lab 7 - Pi File (piUART.py) located in pi_files, lab 6 main located in Lab_06

Final project:
Tiva code- periph_n_blount - periph files 	lab_06/ - main.c 		lab_06 - documentation
Raspberry pi - pi_files/piUART.py - code	pi_files/Noah_Documentation.docx - documentation
UART packet protocol - Packet_Protocol.xlsx
Pin assignments - PINOUT.xlsx

Prelabs:
Noah:
Lab 2 - Lab_02/Noah/Pre_lab_2.pdf
Lab 3 - Lab_03/Noah/Prelab 3.docx
Lab 5 - Lab_05/Noah/Prelab 5.docx

Seth:
Lab 2 -Lab_02/Seth/Prelab2.txt
Lab 3 -Lab_03/Seth/Prelab3.txt
Lab 5 -Lab_05/Seth/ prelab5.txt = psudeocode	prelab5.pdf = state diagram