#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "pwm.h"
#include "motor.h"
#include "quadEnc.h"
#include "timer.h"
#include "ir.h"

/**
 * main.c
 */

void delay()
{
    for( uint32_t i = 0; i < 250000; ++i );
}

void longDelay() //delays for roughly a second
{
    for( uint8_t i = 0; i < 6; ++i )
    {
        delay();
    }
}


int main(void)
{
    pwmInit();
    pwmSetCycle(0, 0);
    pwmEnable();
    motorInit(); //Initialization of required peripherals
    initEncoder();
    initTimer();
    irInit();


    uint32_t lADCVal = 0;
    uint32_t mADCVal = 0;
    uint32_t rADCVal = 0;
    motorForward(true, true, 200, 200);
    while(1)
    {
        lADCVal = getLeftSensorCurrVal();
        mADCVal = getMiddleSensorCurrVal();
        rADCVal = getRightSensorCurrVal();
        if(lADCVal >= 2000)
        {
            motorStopBrake(true, true);
            delay();
            motorBackward(true, true, 200, 200);
            longDelay();
            motorStopBrake(true, true);
            delay();
            motorRight(200, 200);
            delay();
            motorStopBrake(true, true);
            delay();
        }
        else if (mADCVal >= 2000)
        {
            if(lADCVal > rADCVal)
            {
                motorStopBrake(true, true);
                delay();
                motorBackward(true, true, 200, 200);
                longDelay();
                motorStopBrake(true, true);
                delay();
                motorRight(200, 200);
                delay();
                motorStopBrake(true, true);
                delay();
            }
            else
            {
                motorStopBrake(true, true);
                delay();
                motorBackward(true, true, 200, 200);
                longDelay();
                motorStopBrake(true, true);
                delay();
                motorLeft(200, 200);
                delay();
                motorStopBrake(true, true);
                delay();
            }
        }
        else if (rADCVal >= 2000)
        {
            motorStopBrake(true, true);
            delay();
            motorBackward(true, true, 200, 200);
            longDelay();
            motorStopBrake(true, true);
            delay();
            motorLeft(200, 200);
            delay();
            motorStopBrake(true, true);
            delay();
        }
        else
        {
            motorForward(true, true, 200, 200);
            delay();
        }
    }
}
