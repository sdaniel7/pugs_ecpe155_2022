#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
/**
 * main.c
 */
int main(void)
{
  /*
  *PF1 GPIO RGB LED (Red)
  *PF2 GPIO RGB LED (Blue)
  *PF3 GPIO RGD LED (Green)
  */
/*
    #define GPIO_PORT_F     ((volatile uint32_t *)0x40025000)
    enum {
    #define   GPIO_PIN_0              (1 << 0) // pin 0
    #define   GPIO_PIN_1              (1 << 1) // pin 1
    #define   GPIO_PIN_2              (1 << 2) // pin 2
    #define   GPIO_PIN_3              (1 << 3) // pin 3
    #define   GPIO_PIN_4              (1 << 4) // pin 4
    #define   GPIO_PIN_5              (1 << 5) // pin 5
    #define   GPIO_PIN_6              (1 << 6) // pin 6
    #define   GPIO_PIN_7              (1 << 7) // pin 7
    #define   GPIO_ALLPINS            0b11111111 // pins 0 to 7
      GPIO_DIR  =   (0x400 >> 2),
      GPIO_DEN  =   (0x51c >> 2),
    };
    #define SYSCTL          (((volatile uint32_t *)0x400fe000))

    enum {
        SYSCTL_RCGCGPIO =       (0x608 >> 2),
        #define   SYSCTL_RCGCGPIO_PORTF   (1<<5)
    };

    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF;
    GPIO_PORT_F[GPIO_DIR] |= GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;
    GPIO_PORT_F[GPIO_DEN] |= GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;


    while(1)
    {
        for( uint32_t i = 0; i < 250000; ++i );
        GPIO_PORT_F[GPIO_PIN_1] |= GPIO_PIN_1;
        GPIO_PORT_F[GPIO_PIN_2] &= ~GPIO_PIN_2;
        GPIO_PORT_F[GPIO_PIN_3] &= ~GPIO_PIN_3;
        for( uint32_t i = 0; i < 250000; ++i );
        GPIO_PORT_F[GPIO_PIN_1] &= ~GPIO_PIN_1;
        GPIO_PORT_F[GPIO_PIN_2] |= GPIO_PIN_2;
        GPIO_PORT_F[GPIO_PIN_3] &= ~GPIO_PIN_3;
        for( uint32_t i = 0; i < 250000; ++i );
        GPIO_PORT_F[GPIO_PIN_1] &= ~GPIO_PIN_1;
        GPIO_PORT_F[GPIO_PIN_2] &= ~GPIO_PIN_2;
        GPIO_PORT_F[GPIO_PIN_3] |= GPIO_PIN_3;
    }
     */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    while(1)
    {
        for( uint32_t i = 0; i < 250000; ++i );
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_PIN_1);
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, ~GPIO_PIN_2);
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, ~GPIO_PIN_3);
        for( uint32_t i = 0; i < 250000; ++i );
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, ~GPIO_PIN_1);
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, GPIO_PIN_2);
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, ~GPIO_PIN_3);
        for( uint32_t i = 0; i < 250000; ++i );
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, ~GPIO_PIN_1);
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, ~GPIO_PIN_2);
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, GPIO_PIN_3);
    }
}
