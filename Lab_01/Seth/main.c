// Definitions for datatypes used in labs.
#include <stdint.h>
#include <stdbool.h>

// TM4C hardware register and peripheral definitions.
//#include "sysctl.h"
//#include "gpio.h"
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

// State names for finite state machine.
enum States {
  START = 0,
};

// Prototypes for functions in "stuff"
void LEDInit( void );
void noLED( void );
void LEDD2( void );
void LEDD3( void );
void LEDD4( void );
enum States FSM( enum States s );

// Constants needed later
#define CLOSED 0
#define OPEN GPIO_PIN_1

// Dummy finite state machine procedure.
enum States yourFSM( enum States s ) {
  return s;
}
/**
 * main.c
 */
void main( void ) {
  // Turn on run clock gate control for Port N.
  /*SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF;

  // Make PN1 an output and enable it.
  GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;
  GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;



  while( true ) {
    // Step through at a rate we can react to.
    for( uint32_t i = 0; i < 1000000; ++i );  // requires C99 mode

    // Toggle the "clock" LED
    GPIO_PORTF[GPIO_PIN_1] &= ~GPIO_PIN_1;

    for( uint32_t i = 0; i < 1000000; ++i );  // requires C99 mode

    GPIO_PORTF[GPIO_PIN_1] |= GPIO_PIN_1;

    // When the clock LED turns on, execute one cycle of finite state machine
    //if( GPIO_PORTF[GPIO_PIN_1] ) {
    // state = FSM( state );
    }

  }*/
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
        GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
        while(1)
        {
            for( uint32_t i = 0; i < 300000; ++i );
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_PIN_1);
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, ~GPIO_PIN_2);
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, ~GPIO_PIN_3);
            for( uint32_t i = 0; i < 300000; ++i );
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, ~GPIO_PIN_1);
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, GPIO_PIN_2);
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, ~GPIO_PIN_3);
            for( uint32_t i = 0; i < 300000; ++i );
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, ~GPIO_PIN_1);
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, ~GPIO_PIN_2);
            GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, GPIO_PIN_3);
        }

}
