/*
 * gpio.h
 *
 *  Created on: July 29, 2018
 *      Author: khughes
 *
 * GPIO register definitions.  See Chapter 10 of the TM4C1294 datasheet 
 * for complete information.
 */

#ifndef _GPIO_H
#define _GPIO_H

#include <stdint.h>

// Peripheral base addresses.
#define GPIO_PORTD              ((volatile uint32_t *)0x4005B000)
#define GPIO_PORTE              ((volatile uint32_t *)0x4005C000)
#define GPIO_PORTF              ((volatile uint32_t *)0x40025000)
#define GPIO_PORTN              ((volatile uint32_t *)0x40064000)
#define GPIO_PORTJ              ((volatile uint32_t *)0x40060000)

// Peripheral register offsets and special fields
enum {
  GPIO_DIR  =   (0x400 >> 2),
  GPIO_PUR  =   (0x510 >> 2),
  GPIO_DEN  =   (0x51c >> 2),
};

enum {
  GPIO_PIN_0 = (1 << 0),
  GPIO_PIN_1 = (1 << 1),
  GPIO_PIN_2 = (1 << 2),
  GPIO_PIN_3 = (1 << 3),
  GPIO_PIN_4 = (1 << 4),
  GPIO_PIN_5 = (1 << 5),
  GPIO_PIN_6 = (1 << 6),
  GPIO_PIN_7 = (1 << 7),










































































































































































































};

#endif // _GPIO_H
