/*
 * uart.h
 *
 *  Created on: Mar 29, 2022
 *      Author: noahb
 */

#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include <stdio.h>
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/uart.h"



void initUART();
void sendToUART(char *sendData, uint8_t bytesToSend);

#endif /* UART_H_ */
