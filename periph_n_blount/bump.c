/*
 * bump.c
 *
 *  Created on: Feb 23, 2022
 *      Author: noahb
 */

#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "pwm.h"
#include "motor.h"



void bumpInit()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB)){} //enables bump pins and ensures they are fully set up before accessing

    GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_4 | GPIO_PIN_5); //configuring the pins as input

    GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_4);
    GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_5);
}



