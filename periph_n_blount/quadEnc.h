/*
 * quad.h
 *
 *  Created on: Feb 22, 2022
 *      Author: noahb
 */

#ifndef QUAD_H_
#define QUAD_H_

void clearTicks();
void initEncoder();
void leftTickIntHandler();
void rightTickIntHandler();


#endif /* QUAD_H_ */
