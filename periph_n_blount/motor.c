/*
 * motor.c
 *
 *  Created on: Feb 15, 2022
 *      Author: noahb
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "pwm.h"
#include "motor.h"
#include "timer.h"
#include "quadEnc.h"
extern uint16_t leftTicks, rightTicks;
uint16_t timeCount = 0;
bool secondFlagCorrect = false;
float leftSpeed, rightSpeed, ratio;
uint16_t cycle[2];



void moveXCmForward(uint16_t cm, uint16_t lspeed, uint16_t rspeed)
{
    uint16_t ticksPerRot = 64;
    uint16_t cmPerRot = 19;
    uint16_t ticksToMove = ((cm * ticksPerRot) / cmPerRot);

    motorForward(true, true, lspeed, rspeed);
    while((ticksToMove > leftTicks) | (ticksToMove > rightTicks)){}
    clearTicks();
    motorStopBrake(true, true);
}

void moveXCmBackward(uint16_t cm, uint16_t lspeed, uint16_t rspeed)
{
    uint16_t ticksPerRot = 64;
    uint16_t cmPerRot = 19;
    uint16_t ticksToMove = ((cm * ticksPerRot) / cmPerRot);

    motorBackward(true, true, lspeed, rspeed);
    while((ticksToMove > leftTicks) | (ticksToMove > rightTicks)){}
    clearTicks();
    motorStopBrake(true, true);
}

void moveXTicksForward(uint8_t lTick, uint8_t rTick, uint16_t lspeed, uint16_t rspeed)
{
    motorForward(true, true, lspeed, rspeed);
    while((lTick > leftTicks) & (rTick > rightTicks)){}
    clearTicks();
    motorStopBrake(true, true);
}
void moveXTicksBackward(uint8_t lTick, uint8_t rTick, uint16_t lspeed, uint16_t rspeed)
{
    motorBackward(true, true, lspeed, rspeed);
    while((lTick > leftTicks) & (rTick > rightTicks)){}
    clearTicks();
    motorStopBrake(true, true);
}

void rotateXTicks(bool counterCl, uint8_t cm uint16_t lspeed, uint16_t rspeed)
{
    uint16_t ticksPerRot = 64;
    uint16_t cmPerRot = 19;
    uint16_t rTicks = ((cm * ticksPerRot) / cmPerRot);
    uint16_t lTicks = ((cm * ticksPerRot) / cmPerRot);

    if (counterCL == true){
        motorRight(true, true, lspeed, rspeed);
        while((lTick < leftTicks) & (rTick < rightTicks)){}
        clearTicks();
        motorStopBrake(true, true);
    }
    else{
        motorLeft(true, true, lspeed, rspeed);
        while((lTick < leftTicks) & (rTick < rightTicks)){}
        clearTicks();
        motorStopBrake(true, true);
        }
}

void rotateThetaDegsCClock(int16_t degrees, uint16_t lspeed, uint16_t rspeed)
{
    uint16_t bwnWheelRadius = 5;
    uint16_t absOfDeg;
    if(degrees >= 0)
    {
        absOfDeg = degrees;
    }
    else
    {
        absOfDeg = -degrees;
    }

    uint16_t arcLength = ((absOfDeg * bwnWheelRadius * 3) / 180);
    uint16_t ticksPerRot = 64;
    uint16_t cmPerRot = 19;
    uint16_t ticksToMove = ((arcLength * ticksPerRot) / (cmPerRot * 2));

    if(degrees >= 0)
    {
        motorLeft(lspeed, rspeed);
        while((ticksToMove > leftTicks) | (ticksToMove > rightTicks)){}
        clearTicks();
        motorStopBrake(true, true);
    }
    else
    {
        motorRight(lspeed, rspeed);
        while((ticksToMove > leftTicks) | (ticksToMove > rightTicks)){}
        clearTicks();
        motorStopBrake(true, true);
    }
}


void driveXYBox(uint16_t xCm, uint16_t yCm)
{
    rotateThetaDegsCClock(90, 200, 200);
    moveXCm(xCm, 200, 200); //turn 90 degrees left, and move x

    rotateThetaDegsCClock(90, 200, 200);
    moveXCm(yCm, 200, 200); //turn 90 degrees left, and move y

    rotateThetaDegsCClock(90, 200, 200);
    moveXCm(xCm, 200, 200); //turn 90 degrees left, and move x again

    rotateThetaDegsCClock(90, 200, 200);
    moveXCm(yCm, 200, 200); //turn 90 degrees left, and move y again, finishing the rectangle
}
/*
void motorCorrect()
{
    if(secondFlagCorrect)
    {
        secondFlagCorrect = false;
        pwmGetCycle(cycle);

        leftSpeed = getWheelVelocity(true);
        rightSpeed = getWheelVelocity(false); //get wheel speeds

        if(leftSpeed > 0 & rightSpeed > 0) //as long as wheels are moving slightly
        {
            ratio = leftSpeed/rightSpeed; //find ratio of wheel speeds
        }
        else
        {
            ratio = 1;
        }

        if (ratio >= 1)
        {
            if(cycle[1] * ratio >= 375) //prevent speed adjustment from exceeding pulse width
            {
                pwmSetCycle((cycle[0] / ratio), cycle[1]); //dynamically adjust wheel speed to have ratio approach 1
            }
            else
            {
                pwmSetCycle(cycle[0], (cycle[1] * ratio));
            }

        }
        else
        {
            if(cycle[0] / ratio >= 375)
            {
                pwmSetCycle(cycle[0], (cycle[1] * ratio));
            }
            else
            {
                pwmSetCycle((cycle[0] / ratio), cycle[1]);
            }

        }
    }
}
*/
void motorInit()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA)){} //enabling pin outputs on port A for Standby, IN1, and IN2 on the wheels

    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_6 | GPIO_PIN_7); //configuring the pins as output

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)){} //enabling pin outputs on port E for Standby, IN1, and IN2 on the wheels

    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4); //configuring the pins as output

    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_4, GPIO_PIN_4);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_1, GPIO_PIN_1); //enabling the standby pins
}

void motorForward(bool left, bool right, uint16_t lspeed, uint16_t rspeed)
{
    pwmSetCycle(lspeed, rspeed); //set pwm cycle to desired speed percent
    if (left)
    {
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6, ~GPIO_PIN_6); //set IN1 and IN2 to set left wheel to turn backwards (because the wheel is on the left, backwards really means forwards)
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_PIN_7);
    }
    if (right)
    {
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_2, GPIO_PIN_2); //set IN1 and IN2 to set right wheel to turn forwards
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, ~GPIO_PIN_3);
    }
}

void motorBackward(bool left, bool right, uint16_t lspeed, uint16_t rspeed)
{
    pwmSetCycle(lspeed, rspeed); //set pwm cycle to desired speed percent
    if (left)
    {
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6, GPIO_PIN_6); //set IN1 and IN2 to set left wheel to turn forwards (because the wheel is on the left, forwards really means backwards)
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7, ~GPIO_PIN_7);
    }
    if (right)
    {
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_2, ~GPIO_PIN_2); //set IN1 and IN2 to set right wheel to turn backwards
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, GPIO_PIN_3);
    }
}

void motorLeft(uint16_t lspeed, uint16_t rspeed)
{
    motorForward(false, true, lspeed, rspeed);
    motorBackward(true, false, lspeed, rspeed);
}

void motorRight(uint16_t lspeed, uint16_t rspeed)
{
    motorForward(true, false, lspeed, rspeed);
    motorBackward(false, true, lspeed, rspeed);
}

void motorStopBrake(bool left, bool right)
{
    if (left)
    {
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6, GPIO_PIN_6); //set IN1 and IN2 to stop the left wheel with brake
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_PIN_7);
    }
    if (right)
    {
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_2, GPIO_PIN_2); //set IN1 and IN2 to stop the right wheel with brake
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, GPIO_PIN_3);
    }
}

void motorStopFree(bool left, bool right)
{
    if (left)
    {
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6, GPIO_PIN_6); //set IN1 and IN2 to stop the left wheel without brake
        GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_PIN_7);
    }
    if (right)
    {
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_2, GPIO_PIN_2); //set IN1 and IN2 to stop the right wheel without brake
        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_3, GPIO_PIN_3);
    }
}
