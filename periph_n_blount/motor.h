/*
 * motor.h
 *
 *  Created on: Feb 15, 2022
 *      Author: noahb
 */

#ifndef MOTOR_H_
#define MOTOR_H_


void moveXCmForward(uint16_t cm, uint16_t lspeed, uint16_t rspeed);
void moveXCmBackward(uint16_t cm, uint16_t lspeed, uint16_t rspeed);
void moveXTicksForward(uint8_t lTick, uint8_t rTick, uint16_t lspeed, uint16_t rspeed);
void moveXTicksBackward(uint8_t lTick, uint8_t rTick, uint16_t lspeed, uint16_t rspeed);
void rotateXCm(bool counterCl, uint8_t lCm, uint8_t rCm, uint16_t lspeed, uint16_t rspeed);
void rotateThetaDegsCClock(int16_t degrees, uint16_t lspeed, uint16_t rspeed);
void driveXYBox(uint16_t xCm, uint16_t yCm);
void motorInit();
void motorForward(bool left, bool right, uint16_t lspeed, uint16_t rspeed);
void motorBackward(bool left, bool right, uint16_t lspeed, uint16_t rspeed);
void motorLeft(uint16_t lspeed, uint16_t rspeed);
void motorRight(uint16_t lspeed, uint16_t rspeed);
void motorStopBrake(bool left, bool right);
void motorStopFree(bool left, bool right);


#endif /* MOTOR_H_ */
