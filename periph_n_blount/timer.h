/*
 * timer.h
 *
 *  Created on: Feb 22, 2022
 *      Author: noahb
 */

#ifndef TIMER_H_
#define TIMER_H_

void initTimer();
float getCurrHalfSecFraction();
void timerInterruptHandler();

#endif /* TIMER_H_ */
