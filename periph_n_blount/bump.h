/*
 * bump.h
 *
 *  Created on: Feb 23, 2022
 *      Author: noahb
 */

#ifndef BUMP_H_
#define BUMP_H_


void bumpInit();
void bumpCheck();


#endif /* BUMP_H_ */
