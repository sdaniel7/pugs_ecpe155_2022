#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "pwm.h"
#include "motor.h"

/**
 * main.c
 */
void delay()
{
    for( uint32_t i = 0; i < 250000; ++i );
}

void longDelay()
{
    for( uint32_t i = 0; i < 500; ++i )
    {
        delay();
    }
}


int main(void)
{
    pwmInit();
    pwmSetCycle(0, 0);
    pwmEnable();
    motorInit();
    motorForward(true, true, 50, 50);
    longDelay();
    motorStop(true, true);
    longDelay();
    motorBackward(true, true, 50, 50);
    longDelay();
    motorStop(true, true);
    while(1){}
}
