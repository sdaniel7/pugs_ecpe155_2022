/*
 * ir.h
 *
 *  Created on: Mar 19, 2022
 *      Author: noahb
 */

#ifndef ACCEL_H_
#define ACCEL_H_


void accelInit();
uint16_t getSensorCurrVal_X();
uint16_t getSensorCurrVal_Y();


#endif /* IR_H_ */
