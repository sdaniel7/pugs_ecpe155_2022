/*
 * ir.h
 *
 *  Created on: Mar 19, 2022
 *      Author: noahb
 */

#ifndef IR_H_
#define IR_H_


void irInit();
uint16_t getSensorCurrVal_0();
uint16_t getSensorCurrVal_1();
uint16_t getSensorCurrVal_2();
uint16_t getSensorCurrVal_3();
uint16_t getSensorCurrVal_4();
uint16_t getSensorCurrVal_5();


#endif /* IR_H_ */
