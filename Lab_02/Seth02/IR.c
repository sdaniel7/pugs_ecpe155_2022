/*
 * ir.c
 *
 *  Created on: Mar 19, 2022
 *      Author: noahb
 */


#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/adc.h"
#include "ir.h"

#define PART_TM4c123GH6PM
#define GPIO_PORTB          ((volatile uint32_t *)0x40005000)
#define GPIO_PORTD          ((volatile uint32_t *)0x40007000)
#define GPIO_PORTE          ((volatile uint32_t *)0x40024000)
#define ADC0                ((volatile uint32_t *)0x40038000)
enum{
#define PIN_1           (1<<1)
#define PIN_2           (1<<2)
#define PIN_3           (1<<3)
#define PIN_4           (1<<4)
#define PIN_5           (1<<5)
    GPIO_AFSEL = (0x420 >> 2),
    ADCSSMUX0  = (0x040 >> 2)
};


uint32_t DataBuffer_0, DataBuffer_1, DataBuffer_2, DataBuffer_3, DataBuffer_4, DataBuffer_5;
void irInit()
{

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)){}
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD)){} //enables ir pins and ensures they are fully set up before accessing
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB)){}

    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)){} //enable ADC0 and wait for it to be ready
    //
    // Enable the first sample sequencer to capture the value of channel 0 when
    // the processor trigger occurs.
    //

    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_4 | GPIO_PIN_5);
    GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_2 | GPIO_PIN_3); //configuring the pins as input
    GPIOPinTypeADC(GPIO_PORTB_BASE, GPIO_PIN_4);


    GPIO_PORTB[GPIO_AFSEL] |= PIN_4;
    GPIO_PORTD[GPIO_AFSEL] |= PIN_2;
    GPIO_PORTD[GPIO_AFSEL] |= PIN_3;
    GPIO_PORTE[GPIO_AFSEL] |= PIN_1;
    GPIO_PORTE[GPIO_AFSEL] |= PIN_4;
    GPIO_PORTE[GPIO_AFSEL] |= PIN_5; //enabling alternate pin functions


    ADCSequenceDisable(ADC0_BASE, 0);
    ADCSequenceDisable(ADC0_BASE, 1); //sequence 0 = left, 1 = center, 2 = right
    ADCSequenceDisable(ADC0_BASE, 2);//disables ADC to configure it
    ADCSequenceDisable(ADC0_BASE, 3);
    ADCSequenceDisable(ADC0_BASE, 4);
    ADCSequenceDisable(ADC0_BASE, 5);

    ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 1);
    ADCSequenceConfigure(ADC0_BASE, 2, ADC_TRIGGER_PROCESSOR, 2);
    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 3);
    ADCSequenceConfigure(ADC0_BASE, 4, ADC_TRIGGER_PROCESSOR, 4);
    ADCSequenceConfigure(ADC0_BASE, 5, ADC_TRIGGER_PROCESSOR, 5);//sets ADC to capture data when it is polled

    ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_CH5 | ADC_CTL_END);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH4 | ADC_CTL_END);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 0, ADC_CTL_CH8 | ADC_CTL_END); //configure the three channels to be polled
    ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH1 | ADC_CTL_END);
    ADCSequenceStepConfigure(ADC0_BASE, 4, 0, ADC_CTL_CH2 | ADC_CTL_END);
    ADCSequenceStepConfigure(ADC0_BASE, 5, 0, ADC_CTL_CH10 | ADC_CTL_END);

    ADCSequenceEnable(ADC0_BASE, 0);
    ADCSequenceEnable(ADC0_BASE, 1);
    ADCSequenceEnable(ADC0_BASE, 2);
    ADCSequenceEnable(ADC0_BASE, 3);
    ADCSequenceEnable(ADC0_BASE, 4);
    ADCSequenceEnable(ADC0_BASE, 5);

    ADC0[ADCSSMUX0] |= (5 << 0); //sets AIN5 to be polled first (left IR)
    ADC0[ADCSSMUX0] |= (4 << 4); //sets AIN4 to be polled second (front IR)
    ADC0[ADCSSMUX0] |= (8 << 8); //sets AIN8 to be polled last (right IR)
    ADC0[ADCSSMUX0] |= (1 << 12);
    ADC0[ADCSSMUX0] |= (2 << 16);
    ADC0[ADCSSMUX0] |= (10 << 20);

}

uint16_t getSensorCurrVal_0()
{
    ADCProcessorTrigger(ADC0_BASE, 0); //poll the ADC left to get the current values
    ADCSequenceDataGet(ADC0_BASE, 0, &DataBuffer_0); //transfer the current value to the left dataBuffer
    return DataBuffer_0; //return the current ADC value
}

uint16_t getSensorCurrVal_1()
{
    ADCProcessorTrigger(ADC0_BASE, 1); //poll the ADC to get the current values
    ADCSequenceDataGet(ADC0_BASE, 1, &DataBuffer_1); //transfer the current value to the middle dataBuffer
    return DataBuffer_1; //return the current ADC value
}

uint16_t getSensorCurrVal_2()
{
    ADCProcessorTrigger(ADC0_BASE, 2); //poll the ADC to get the right current values
    ADCSequenceDataGet(ADC0_BASE, 2, &DataBuffer_2); //transfer the current value to the right dataBuffer
    return DataBuffer_2; //return the current ADC value
}

uint16_t getSensorCurrVal_3()
{
    ADCProcessorTrigger(ADC0_BASE, 3); //poll the ADC to get the right current values
    ADCSequenceDataGet(ADC0_BASE, 3, &DataBuffer_3); //transfer the current value to the right dataBuffer
    return DataBuffer_3; //return the current ADC value
}

uint16_t getSensorCurrVal_4()
{
    ADCProcessorTrigger(ADC0_BASE, 4); //poll the ADC to get the right current values
    ADCSequenceDataGet(ADC0_BASE, 4, &DataBuffer_4); //transfer the current value to the right dataBuffer
    return DataBuffer_4; //return the current ADC value
}

uint16_t getSensorCurrVal_5()
{
    ADCProcessorTrigger(ADC0_BASE, 5); //poll the ADC to get the right current values
    ADCSequenceDataGet(ADC0_BASE, 5, &DataBuffer_5); //transfer the current value to the right dataBuffer
    return DataBuffer_5; //return the current ADC value
}

