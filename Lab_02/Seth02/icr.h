/*
 * icr.h
 *
 *  Created on: Feb 18, 2022
 *      Author: noahb
 */

#ifndef ICR_H_
#define ICR_H_

float timeToRotate(float theta);
float timeToMove(uint8_t xCm, uint8_t yCm);


#endif /* ICR_H_ */
