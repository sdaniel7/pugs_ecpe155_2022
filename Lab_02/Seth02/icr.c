/*
 * icr.c
 *
 *  Created on: Feb 18, 2022
 *      Author: noahb
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "pwm.h"
#include "quadEnc.h"
#include "icr.h"

float radius = 3; //radius is 3 cm
float circumference = 19;
float length = 5; //l is 5 cm
float phi1, phi2;
float alpha = .296875; //alpha = circumference/ticks, circumference is 19 cm, 64 ticks in a rotation


float timeToRotate(float theta)
{
    phi1 = getWheelVelocity(true);
    phi2 = getWheelVelocity(false); //get current wheel velocities

    float time1, time2, numerator, denominator;

    numerator = abs(theta - (2 * length));
    denominator = phi1 * radius; //solve "turn first" equation for delta t

    time1 = numerator/denominator; //calculate first time with left wheel

    denominator = phi2 * radius;

    time2 = numerator/denominator; //calculate second time with right wheel

    time1 = (time1 + time2) / 2; //average the two times for best result
    return time1;
}

float timeToMove(uint8_t xCm, uint8_t yCm)
{
    phi1 = getWheelVelocity(true);
    phi2 = getWheelVelocity(false); //get current wheel velocities

    float time1, time2, distance, numerator, denominator;

    distance = sqrt((xCm * xCm) + (yCm * yCm)); //calculate the distance needed to move

    numerator = 2 * alpha * distance;
    denominator = phi1 * radius; //solve "then move" equation for delta t

    time1 = numerator/denominator; //calculate first time with left wheel

    denominator = phi2 * radius;

    time2 = numerator/denominator; //calculate second time with right wheel

    time1 = (time1 + time2) / 2; //average the two times for best result
    return time1;
}
