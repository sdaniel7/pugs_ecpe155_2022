/*
 * uart.c
 *
 *  Created on: Mar 29, 2022
 *      Author: noahb
 */
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include <stdio.h>
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/uart.h"
#include "uart.h"


void initUART()
{
    SysCtlClockSet( SYSCTL_USE_OSC | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ | SYSCTL_SYSDIV_1 );
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    GPIOPinConfigure(GPIO_PB0_U1RX);
    GPIOPinConfigure(GPIO_PB1_U1TX);
    GPIOPinTypeUART(GPIO_PORTB_BASE, (GPIO_PIN_0 | GPIO_PIN_1));

    UARTConfigSetExpClk(UART1_BASE, SysCtlClockGet(), 9600, UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE);
}

void sendToUART(char *sendData, uint8_t bytesToSend)
{
    //uint8_t sizeOfDat = sizeof(sendData) / sizeof(sendData[0]);
    for (uint8_t i = 0; i < bytesToSend; i++)
    {
        UARTCharPut(UART1_BASE,sendData[i]);
    }

}


