/*
 * ir.c
 *
 *  Created on: Mar 19, 2022
 *      Author: noahb
 */


#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/adc.h"
#include "ir.h"

#define PART_TM4c123GH6PM
#define GPIO_PORTE          ((volatile uint32_t *)0x40024000)
#define ADC0                ((volatile uint32_t *)0x40038000)
enum{
#define PIN_2           (1<<2)
#define PIN_3           (1<<3)
    GPIO_AFSEL = (0x420 >> 2),
    ADCSSMUX0  = (0x040 >> 2)
};


uint32_t DataBuffer_X, DataBuffer_Y, DataBuffer_St;
void accelInit()
{

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE)){}

    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0)){} //enable ADC0 and wait for it to be ready
    //
    // Enable the first sample sequencer to capture the value of channel 0 when
    // the processor trigger occurs.
    //

    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2| GPIO_PIN_3);//configuring the pins as input


    GPIO_PORTE[GPIO_AFSEL] |= PIN_2;
    GPIO_PORTE[GPIO_AFSEL] |= PIN_3; //enabling alternate pin functions


    ADCSequenceDisable(ADC0_BASE, 0);
    ADCSequenceDisable(ADC0_BASE, 1); //sequence 0 = left, 1 = center, 2 = right //disables ADC to configure it

    ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 1);//sets ADC to capture data when it is polled

    ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_CH0| ADC_CTL_END);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH1 | ADC_CTL_END); //configure the three channels to be polled

    ADCSequenceEnable(ADC0_BASE, 0);
    ADCSequenceEnable(ADC0_BASE, 1);

    ADC0[ADCSSMUX0] |= (0 << 0); //sets AIN0 to be polled first
    ADC0[ADCSSMUX0] |= (1 << 4); //sets AIN1 to be polled second

}

uint16_t getSensorCurrVal_X()
{
    ADCProcessorTrigger(ADC0_BASE, 0); //poll the ADC left to get the current values
    ADCSequenceDataGet(ADC0_BASE, 0, &DataBuffer_X); //transfer the current value to the left dataBuffer
    return DataBuffer_X; //return the current ADC value
}

uint16_t getSensorCurrVal_Y()
{
    ADCProcessorTrigger(ADC0_BASE, 1); //poll the ADC to get the current values
    ADCSequenceDataGet(ADC0_BASE, 1, &DataBuffer_Y); //transfer the current value to the middle dataBuffer
    return DataBuffer_Y; //return the current ADC value
}

