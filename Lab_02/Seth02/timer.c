/*
 * timer.c
 *
 *  Created on: Feb 22, 2022
 *      Author: noahb
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/timer.h"
#include "timer.h"
#include "pwm.h"
#include "quadEnc.h"

extern uint16_t timeCount;
extern bool secondFlagCorrect;
bool light = false;

void initTimer()
{
    uint32_t clockSize = SysCtlClockGet() / 2;
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0); //enable timer 0 and wait for it to be ready
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0)){}


    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);


    TimerLoadSet(TIMER0_BASE, TIMER_A, clockSize);


    TimerIntRegister(TIMER0_BASE, TIMER_A, timerInterruptHandler);

    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    TimerEnable(TIMER0_BASE, TIMER_A);



    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF)){}
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);

}

float getCurrHalfSecFraction()
{
    uint32_t clockSize = SysCtlClockGet() / 2;
    uint32_t currClockCount = TimerValueGet(TIMER0_BASE, TIMER_A);
    return currClockCount / clockSize;
}

void timerInterruptHandler()
{
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    secondFlagCorrect = true;
    timeCount += 1;
    if (light)
    {
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, ~GPIO_PIN_1);
        light = false;
    }
    else
    {
       GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_PIN_1);
       light = true;
    }
}

