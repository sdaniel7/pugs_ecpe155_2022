/*
 * quad.c
 *
 *  Created on: Feb 22, 2022
 *      Author: noahb
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/qei.h"
#include "driverlib/pin_map.h"
#include "quadEnc.h"

uint16_t leftTicks, rightTicks;

void clearTicks()
{
    leftTicks = 0;
    rightTicks = 0;
}

void initEncoder()
{

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD)){}
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOC)){} //enables QE pins and ensures they are fully set up before accessing

    GPIOIntRegister(GPIO_PORTD_BASE, leftTickIntHandler);
    GPIOIntRegister(GPIO_PORTC_BASE, rightTickIntHandler); //set up interrupts for each pin

    GPIOPinTypeGPIOInput(GPIO_PORTD_BASE, GPIO_PIN_6); //set pin D6 as left wheel gpio input
    GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_5); //set pin C5 as right wheel gpio input

    GPIOIntTypeSet(GPIO_PORTD_BASE, GPIO_PIN_6, GPIO_BOTH_EDGES);
    GPIOIntTypeSet(GPIO_PORTC_BASE, GPIO_PIN_5, GPIO_BOTH_EDGES); //set the encoder to detect both edges of the signal to double resolution

    GPIOIntEnable(GPIO_PORTD_BASE, GPIO_PIN_6);
    GPIOIntEnable(GPIO_PORTC_BASE, GPIO_PIN_5); //enable the pins for the encoder

    clearTicks();
}

void leftTickIntHandler()
{
    leftTicks += 1;
    GPIOIntClear(GPIO_PORTD_BASE, GPIO_PIN_6);
}

void rightTickIntHandler()
{
    rightTicks += 1;
    GPIOIntClear(GPIO_PORTC_BASE, GPIO_PIN_5);
}

/* GPIOIntClear(base, pins);
float getWheelVelocity(bool leftOrRight) //left is true, right is false
{
    uint32_t lVelocity = QEIVelocityGet(QEI0_BASE);
    uint32_t rVelocity = QEIVelocityGet(QEI1_BASE);
    float_t pulsePerRevolution = 64;
    float_t answer;
    float_t twoPi = 6.283185;
    if(leftOrRight)
    {
        answer = (lVelocity/(pulsePerRevolution)) * twoPi; //converts left QEI velocity into rads/second
    }
    else
    {
        answer = (rVelocity/(pulsePerRevolution)) * twoPi; //converts right QEI velocity into rads/second
    }
    return answer;
}
*/
