/*
 * pwm.c
 *
 *  Created on: Feb 15, 2022
 *      Author: noahb
 */

#include <stdint.h>
#include <stdbool.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "pwm.h"

void pwmInit()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOD)){} //enables PWM pins and ensures they are fully set up before accessing

    GPIOPinTypePWM(GPIO_PORTD_BASE, (GPIO_PIN_0 | GPIO_PIN_1)); //set pin D0 and D1 as PWM outputs
    GPIOPinConfigure(GPIO_PD0_M1PWM0);
    GPIOPinConfigure(GPIO_PD1_M1PWM1);

    SysCtlPWMClockSet(SYSCTL_PWMDIV_1); //set PWM clock

    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM1)){} //enabling PWM modules and ensures they are fully set up before proceeding


    PWMGenConfigure(PWM1_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC); //setting PWM to countdown mode
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_0, 400); //set duty length to 400 ticks
}

void pwmSetCycle(uint16_t lpercent, uint16_t rpercent)
{
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_0, (lpercent)); //setting pulse width to amount desired
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_1, (rpercent));
}

void pwmGetCycle(uint16_t cycle[2])
{
    cycle[0] = (PWMPulseWidthGet(PWM1_BASE, PWM_OUT_0)); //getting pulse width
    cycle[1] = (PWMPulseWidthGet(PWM1_BASE, PWM_OUT_1));
}

void pwmEnable()
{
    PWMGenEnable(PWM1_BASE, PWM_GEN_0);
    PWMOutputState(PWM1_BASE, (PWM_OUT_0_BIT | PWM_OUT_1_BIT), true); //enabling PWM output
}

