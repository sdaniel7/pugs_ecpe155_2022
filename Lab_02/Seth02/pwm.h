/*
 * pwm.h
 *
 *  Created on: Feb 15, 2022
 *      Author: noahb
 */

#ifndef PWM_H_
#define PWM_H_


void pwmInit();
void pwmSetCycle(uint16_t lpercent, uint16_t rpercent);
void pwmGetCycle(uint16_t cycle[2]);
void pwmEnable();


#endif /* PWM_H_ */
