import serial
import struct
import math
import numpy as np
import time
ser = serial.Serial("/dev/ttyS0", 9600)








#--------------------------------------------------------------------------------------------------------------

# ------Initializing variables used:------

vfhGrid = np.zeros((18, 32)) #Array of 32 * 18 for obstacle avoidance (python uses y,x notation) See excel sheet for sensor data grid values
forceArray = np.zeros((2))

globalPos = [0.0, 0.0, 0.0]
globalPosToMove = [0.0, 0.0, 0.0]
localPosToMove = [0.0, 0.0, 0.0]
objectPos = [0.0, 0.0]
currPos = [0.0, 0.0, 0.0]

userInput = 0

betweenWheelRadius = 4.5
ticksPerRot = 64
cmPerRot = 19.7

wheelSpeed = 175 & 0xffff

quitCom = False
obstAvoid = True
moving = False

sensor1Dat = 0
sensor2Dat = 0
sensor3Dat = 0
sensor4Dat = 0
sensor5Dat = 0
sensor6Dat = 0

sensor1Dist = 0
sensor2Dist = 0
sensor3Dist = 0
sensor4Dist = 0
sensor5Dist = 0
sensor6Dist = 0

adcMin = 0
adcMax = 4095

accelMin = -3000
accelMax = 3000

scalar = (adcMax - adcMin) / (accelMax - accelMin)

sampleFreq = 240
cutoffFreq = 1 / (2 * sampleFreq)
ewmaA = ((2 * math.pi) * cutoffFreq) / ((2 * math.pi) * cutoffFreq + 1)

accelerationX = np.zeros((10,2)) #Array of 2 * 10, stores 10 most recent acceleration measurements, and the time they were measured
accelerationY = np.zeros((10,2))
accelerationXScaled = np.zeros((10,2)) #Array of 2 * 10, stores 10 most recent acceleration measurements, scaled to milli-g's
accelerationYScaled = np.zeros((10,2))

velocityX = np.zeros((9,2)) #Array of 2 * 9 values of the most recent velocities
velocityY = np.zeros((9,2))

displacementX = np.zeros((8,2))
displacementY = np.zeros((8,2))


leftTicksMoved = 0
rightTicksMoved = 0

leftCmMoved = 0
rightCmMoved = 0



# ------Commands:------

startCommand = 0xAA
endCommand = 0x55

specialComm = 0xDD

movFd = 0x00
movBk = 0x01
turnL = 0x02
turnR = 0x03
stopB = 0x04
stopF = 0x05

chkMove = 0x0F
moveDist = 0x07
movOvr = 0x06

irSensorPoll = 0x10
sensor1 = 0x11
sensor2 = 0x12
sensor3 = 0x13
sensor4 = 0x14
sensor5 = 0x15
sensor6 = 0x16

accelPoll = 0x1F
accelX = 0x1E
accelY = 0x1D

bump = 0x1F

pollMove = struct.pack('BBB', startCommand, chkMove, endCommand)
pollIR = struct.pack('BBB', startCommand, irSensorPoll, endCommand)
pollAccel = struct.pack('BBB', startCommand, accelPoll, endCommand)
specialCommand = struct.pack('BBB', startCommand, specialComm, endCommand)

#--------------------------------------------------------------------------------------------------------------

def scaleXAccel(): #Scale all of the stored acceleration x values and store them
    for i in range(0, 10):
        accelerationXScaled[i, 1] = accelerationX[i, 1]
        accelerationXScaled[i, 0] = (accelerationX[i, 0] - adcMin) / scalar + accelMin

#--------------------------------------------------------------------------------------------------------------

def scaleYAccel(): #Scale all of the stored acceleration y values and save them
    for i in range(0,10):
        accelerationYScaled[i, 1] = accelerationY[i, 1]
        accelerationYScaled[i, 0] = (accelerationY[i, 0] - adcMin) / scalar + accelMin

#--------------------------------------------------------------------------------------------------------------

def updateYAccelDat(data, time): #Push all previously stored y accelerations back an index and store the most recent measurement in index 0. Uses EWMA
    for i in range(1, 10):
        accelerationY[10 - i] = accelerationY[9 - i]
    accelerationY[0, 0] = (ewmaA * data) + ((1 - ewmaA) * accelerationY[1, 0])
    accelerationY[0, 1] = time
    scaleYAccel()

#--------------------------------------------------------------------------------------------------------------

def updateXAccelDat(data, time): #Push all previously stored x accelerations back an index and store the most recent measurement in index 0. Uses EWMA
    for i in range(1, 10):
        accelerationX[10 - i] = accelerationX[9 - i]
    accelerationX[0, 0] = (ewmaA * data) + ((1 - ewmaA) * accelerationX[1, 0])
    accelerationX[0, 1] = time
    scaleXAccel()

#--------------------------------------------------------------------------------------------------------------

def updateIRDist(): #Convert sensor data to distances
    sensor1Dist = valToCm(sensor1Dat)
    sensor2Dist = valToCm(sensor2Dat)
    sensor3Dist = valToCm(sensor3Dat)
    sensor4Dist = valToCm(sensor4Dat)
    sensor5Dist = valToCm(sensor5Dat)
    sensor6Dist = valToCm(sensor6Dat)

#--------------------------------------------------------------------------------------------------------------

def valToCm(irVal): #Convert the ADC IR value into a distance in cm
    voltage = valToVolt(irVal)
    return voltToCM(voltage)

#--------------------------------------------------------------------------------------------------------------

def valToVolt(irVal): #Convert the ADC IR value to the voltage sent by the sensor
#ADC Voltage Calculation 3.3 / 4096 = 0.00080566
    volts = irVal * .00080566406
    return volts

#--------------------------------------------------------------------------------------------------------------

def voltToCM(voltage): #Using voltage characteristics of IR sensor to convert voltage from sensor into distance
#Rough voltage calculation based on matlab:
# voltage = a-b*exp(-c*cm)
#       a =      0.3998  (Roughly between 0.3332, 0.4664)
#       b =      -4.167  (Roughly between -4.415, -3.919)
#       c =      0.1542  (Roughly between 0.14, 0.1684)
#This function inverses this equation to pull cm with voltage input

    logVar = -4.167 / (-voltage + .3998)
    cm = (log(logVar)) / .1542
    return cm

#--------------------------------------------------------------------------------------------------------------

def displacementFromAccelerations(accel1, accel2, accel3, accel4, time1, time2, time3, time4):
    #Acceleration is in milli-g's. We must first convert it to cm/s2
    accel1 = accel1 * .980665
    accel2 = accel2 * .980665
    accel3 = accel3 * .980665
    accel4 = accel4 * .980665

    velocity1 = (time2 - time1) * ((accel1 + accel2) / 2) #Using trapazoidal integration interpolation to calculate two velocities, then position
    velocity2 = (time4 - time3) * ((accel3 + accel4) / 2)
    return (average(time4, time3) - average(time2, time1)) * ((velocity1 + velocity2) / 2)

#--------------------------------------------------------------------------------------------------------------

def average(num1, num2): #Return the average of two numbers
    return (num1 + num2) / 2

#--------------------------------------------------------------------------------------------------------------

def generateForces(): #Using the VFF model, generates an x-force and a y-force acting on the robot 
    forceConst = 5
    forceArray.fill(0.0)
    createVFHMap()

    for i in range(0, 18):
        for j in range(0, 32): #For each grid square
            distance = math.sqrt((i - 0) ** 2 + (j - 16) ** 2) #Find the distance to the robot's location
            if (distance < 1):
                distance = 1 #To prevent division by zero, change all distances below 1 to 1
            multiplier = (forceConst * vfhGrid[i, j]) / (distance ** 2) #Create a scalar based on the certainty of an object at a given square, inversly proportional to distance
            forceArray[0] -= (((j - 16) / (distance)) * multiplier) #Create an x repulsive force based on the certainty that an object is in square i, j
            forceArray[1] -= (((i - 0) / (distance)) * multiplier) #Create a y repulsive force based on the certainty that an object is in square i, j

    globalDist = math.sqrt((globalPosToMove[0] - currPos[0]) ** 2 + (globalPosToMove[1] - currPos[1]) ** 2) #Calculate the distance from the robot to the desired position
    if (globalDist < 1):
        globalDist = 1 #To prevent division by zero, set all values less than 1 to 1
    forceArray[0] += forceConst * ((globalPosToMove[0] - currPos[0]) / globalDist) #Create an x attractive force to pull the robot to the goal
    forceArray[1] += forceConst * ((globalPosToMove[1] - currPos[1]) / globalDist) #Create a y attractive force to pull the robot to the goal

#--------------------------------------------------------------------------------------------------------------

def polarToCartesianGridUpdate(range, theta, value): #Takes in polar coordinates and a value, adds the value to the corresponding cartesian grid square
    radTheta = theta * (math.pi / 180) #Convert degrees to radians to use with math.trigonometric functions
    x = math.floor((range * math.cos(radTheta)) / 2.5) + 16 #Shift right 16 to move origin to center of sensors
    y = math.floor((range * math.sin(radTheta)) / 2.5) 
    
    vfhGrid[y,x] += value

#--------------------------------------------------------------------------------------------------------------

def incrementGridSquare(sensorNum, distVal): #Takes in a sensor, and the values measured from it, and updates the local VFH grid with the measured values

    if(distVal <= 40 and distVal >= 5):

        if(sensorNum == 1):
            for i in range(154, 176, 4): #Sweeps sensor 1 measuring area in 4 degree increments               
                polarToCartesianGridUpdate(distVal, i, 5) #Adds 5 to the measured distance grid squares
                polarToCartesianGridUpdate(distVal - 2, i, 3) #Adds 3 to the grid squares 2cm closer from the measured distance
                if (distVal > 10):
                    for j in range(0, distVal - 8, 2): #Subracts 1 from grid squares between the measured distance and the robot, as these grid squares are most likely free
                        polarToCartesianGridUpdate(j, i, -1)


        elif(sensorNum == 2):
            for i in range(129, 151, 4): #Repeats the procedure for sensor 1, but for sensor 2
                polarToCartesianGridUpdate(distVal, i, 5)
                polarToCartesianGridUpdate(distVal - 2, i, 3)
                if (distVal > 10):
                    for j in range(0, distVal - 8, 2):
                        polarToCartesianGridUpdate(j, i, -1)


        elif(sensorNum == 3):
            for i in range(89, 111, 4): #Repeats the procedure for sensor 1, but for sensor 3
                polarToCartesianGridUpdate(distVal, i, 5)
                polarToCartesianGridUpdate(distVal - 2, i, 3)
                if (distVal > 10):
                    for j in range(0, distVal - 8, 2):
                        polarToCartesianGridUpdate(j, i, -1)


        elif(sensorNum == 4):
            for i in range(64, 86, 4): #Repeats the procedure for sensor 1, but for sensor 4
                polarToCartesianGridUpdate(distVal, i, 5)
                polarToCartesianGridUpdate(distVal - 2, i, 3)
                if (distVal > 10):
                    for j in range(0, distVal - 8, 2):
                        polarToCartesianGridUpdate(j, i, -1)



        elif(sensorNum == 5):
            for i in range(34, 56, 4): #Repeats the procedure for sensor 1, but for sensor 5
                polarToCartesianGridUpdate(distVal, i, 5)
                polarToCartesianGridUpdate(distVal - 2, i, 3)
                if (distVal > 10):
                    for j in range(0, distVal - 8, 2):
                        polarToCartesianGridUpdate(j, i, -1)



        elif(sensorNum == 6):
            for i in range(14, 36, 4): #Repeats the procedure for sensor 1, but for sensor 6
                polarToCartesianGridUpdate(distVal, i, 5)
                polarToCartesianGridUpdate(distVal - 2, i, 3)
                if (distVal > 10):
                    for j in range(0, distVal - 8, 2):
                        polarToCartesianGridUpdate(j, i, -1)

        else:
            pass #Don't update any grid squares if the sensor number is invalid or the distance measurement is out of range
    else:
        pass

#--------------------------------------------------------------------------------------------------------------

def createVFHMap(): #Generate the vfhGrid values for the given sensor readings

    vfhGrid.fill(0.0)
    ser.write(pollIR)
    
    incrementGridSquare(1, sensor1Dist)
    incrementGridSquare(2, sensor2Dist)
    incrementGridSquare(3, sensor3Dist)
    incrementGridSquare(4, sensor4Dist)
    incrementGridSquare(5, sensor5Dist)
    incrementGridSquare(6, sensor6Dist)


    for y in range(0, 18):
        for x in range(0, 32): #Restrict the vfhGrid values to 15 >= value >= 0 to prevent issues arising in the force value calculations

            if (vfhGrid[y,x] > np.float64(15.0)):
                vfhGrid[y,x] = np.float64(15.0)

            if (vfhGrid[y,x] < np.float64(0.0)):
                vfhGrid[y,x] = np.float64(0.0)

#--------------------------------------------------------------------------------------------------------------

def waitForPkt(): #Idle until a packet is sent by the tiva
    while ((ser.in_waiting <= 0) and moving):
        pass
    
#--------------------------------------------------------------------------------------------------------------

def recieveData(): #Recieve, decode, and process commands send by the tiva
    unpkInput = list(range(0, 16))
    x = 0
    waitForPkt() #Wait for a packet to be sent
    while (ser.in_waiting > 0):
        serInput = ser.read() #Read in the packet data
        serInputList = list(struct.unpack_from('B', serInput)) #Unpack the sensor data
        unpkInput[x] = (serInputList[0:len(serInputList)]) #Add upacked data to an array
        if (unpkInput[x] == endCommand): #If endCommand is recieved, stop reading
            break
        if int (x == 10): #If end command is not recieved after 10 characters have been sent, stop reading
            break
        x+=1    
    
    if (unpkInput[1] == movOvr): #If movement complete is sent, tell program that the robot movement is finished
        moving = False
        print('\nMovement completed.\n')
    
    elif (unpkInput[1] == moveDist): #If move distance is sent, read in the tick values for the distance moved, and convert them to cm to be stored in the cm variable
        leftTicksMoved = ((unpkInput[2] & 0xff) << 8 ) | (unpkInput[3] & 0xff)
        rightTicksMoved = ((unpkInput[4] & 0xff) << 8 ) | (unpkInput[5] & 0xff)
        leftCmMoved = ((cmPerRot * leftTicksMoved) / ticksPerRot)
        rightCmMoved = ((cmPerRot * rightTicksMoved) / ticksPerRot)
    
    elif (unpkInput[1] == sensor1): #If sensor 1 data is read, store it, and read again for sensor 2
        sensor1Dat = ((unpkInput[2] & 0xff) << 8) | (unpkInput[3] & 0xff)
        recieveData()
    
    elif (unpkInput[1] == sensor2): #If sensor 2 data is read, store it, and read again for sensor 3
        sensor2Dat = ((unpkInput[2] & 0xff) << 8) | (unpkInput[3] & 0xff)
        recieveData()
    
    elif (unpkInput[1] == sensor3): #If sensor 3 data is read, store it, and read again for sensor 4
        sensor3Dat = ((unpkInput[2] & 0xff) << 8) | (unpkInput[3] & 0xff)
        recieveData()

    elif (unpkInput[1] == sensor4): #If sensor 4 data is read, store it, and read again for sensor 5
        sensor4Dat = ((unpkInput[2] & 0xff) << 8) | (unpkInput[3] & 0xff)
        recieveData()

    elif (unpkInput[1] == sensor5): #If sensor 5 data is read, store it, and read again for sensor 6
        sensor5Dat = ((unpkInput[2] & 0xff) << 8) | (unpkInput[3] & 0xff)
        recieveData()

    elif (unpkInput[1] == sensor6): #If sensor 6 data is read, store it, and update the distance variables based on the read sensor values
        sensor6Dat = ((unpkInput[2] & 0xff) << 8) | (unpkInput[3] & 0xff)
        updateIRDist()

    elif (unpkInput[1] == bump): #If a bump occured, terminate the movement
        print('\nBump detected. Movement terminated.\n')
        moving = False
    
    elif (unpkInput[1] == accelX): #If accelerometer x data is read, store it, then read again for accelerometer y
        updateXAccelDat((((unpkInput[2] & 0xff) << 8) | (unpkInput[3] & 0xff)), time.clock())
        recieveData()

    elif (unpkInput[1] == accelY): #If accelerometer y data is read, store it
        updateYAccelDat((((unpkInput[2] & 0xff) << 8) | (unpkInput[3] & 0xff)), time.clock())

    else: #If unrecognized data is sent, display it
        print('\nInvalid tiva command. Data recieved:\n')
        print(unpkInput)
        print('\n\n') 
    
#--------------------------------------------------------------------------------------------------------------    

def toggleObst(): #Toggle obstacle avoidance step in movement
    if(obstAvoid):
        print('\nObstacle avoidance is currently enabled. Would you like to disable it? (Y/N): ')
        obsInput = input()
        
        while((obsInput != 'Y') and (obsInput != 'N') and (obsInput != 'y') and (obsInput != 'n')):
            print('\nInvalid input. Please enter "Y" or "N"\n')
            print('\nObstacle avoidance is currently enabled. Would you like to disable it? (Y/N): ')
            obsInput = input()
            
        if((obsInput != 'Y') or (obsInput != 'y')):
            obstAvoid = False
            print('\nObstacle avoidance is disabled.\n')
        else:
            print('\nObstacle avoidance is enabled.\n')
    else:
        print('\nObstacle avoidance is currently disabled. Would you like to enable it? (Y/N): ')
        obsInput = input()
        
        while((obsInput != 'Y') and (obsInput != 'N') and (obsInput != 'y') and (obsInput != 'n')):
            print('\nInvalid input. Please enter "Y" or "N"\n')
            print('\nObstacle avoidance is currently disabled. Would you like to enable it? (Y/N): ')
            obsInput = input()
            
        if((obsInput != 'Y') or (obsInput != 'y')):
            obstAvoid = True
            print('\nObstacle avoidance is enabled.\n')
        else:
            print('\nObstacle avoidance is disabled.\n')   

#--------------------------------------------------------------------------------------------------------------    
   
def moveLocal(X, Y): #Local movement is just displacement from current global position, so local movement can jsut call global movement
    moveGlobal(globalPos[0] + X, globalPos[1] + Y)
    
#--------------------------------------------------------------------------------------------------------------    
    
def moveGlobal(X, Y): #Global movement function
    
    turnDegrees = math.degrees(math.atan2((Y - globalPos[1]), (X - globalPos[0]))) - (90 + globalPos[2]) #Calculate angle between new point and tiva global start position (x, y, theta)
    arcLength = ( 2 * math.pi * betweenWheelRadius ) * ( abs(turnDegrees) / 360 ) #Calculate the arc length for the wheels to turn to rotate that number of degrees
    ticksToRot = round((arcLength * ticksPerRot) / (cmPerRot * 2)) #Calculate how many ticks each wheel needs to turn to rotate the correct amount
    
    globalPosToMove[0] = X
    globalPosToMove[1] = Y
    globalPosToMove[2] = turnDegrees

    if(turnDegrees > 0):
        ser.write(struct.pack('6B', startCommand, turnL, int((wheelSpeed >> 8) & 0xff), int(wheelSpeed & 0xff), ((ticksToRot)), endCommand))
        
    else:
        ser.write(struct.pack('6B', startCommand, turnR, int((wheelSpeed >> 8) & 0xff), int(wheelSpeed & 0xff), ((ticksToRot)), endCommand))
            
    globalPos[2] += turnDegrees
    moving = True
    
    while(moving and globalPos != globalPosToMove):
        ser.write(struct.pack('6B', startCommand, movFd, int((wheelSpeed >> 8) & 0xff), int(wheelSpeed & 0xff), ((2)), endCommand))
        positionEstimation()
        if(obstAvoid):
            obstacleAvoidance()
            
        turnDegrees = math.degrees(math.atan2((Y - globalPos[1]), (X - globalPos[0]))) - (90 + globalPos[2]) #Calculate angle between new point and tiva global start position (x, y, theta)
        arcLength = ( 2 * math.pi * betweenWheelRadius ) * ( abs(turnDegrees) / 360 ) #Calculate the arc length for the wheels to turn to rotate that number of degrees
        ticksToRot = round((arcLength * ticksPerRot) / (cmPerRot * 2)) #Calculate how many ticks each wheel needs to turn to rotate the correct amount
        if(turnDegrees > 0):
            ser.write(struct.pack('6B', startCommand, turnL, int((wheelSpeed >> 8) & 0xff), int(wheelSpeed & 0xff), ((ticksToRot)), endCommand))
        
        else:
            ser.write(struct.pack('6B', startCommand, turnR, int((wheelSpeed >> 8) & 0xff), int(wheelSpeed & 0xff), ((ticksToRot)), endCommand))
        globalPos[2] += turnDegrees
      
#--------------------------------------------------------------------------------------------------------------

def obstacleAvoidance(): #Obstacle avoidance step for movement
    
    generateForces()
    if (forceArray[1] < 0):
        if(forceArray[0] > 0):
            turnDegrees = -90
        else:
            turnDegrees = 90
    elif (forceArray[0] > 0):
        turnDegrees = -30
    elif (forceArray[0] < 0):
        turnDegrees = 30
    else:
        turnDegrees = 0
    
    arcLength = ( 2 * math.pi * betweenWheelRadius ) * ( abs(turnDegrees) / 360 ) #Calculate the arc length for the wheels to turn to rotate that number of degrees
    ticksToRot = round((arcLength * ticksPerRot) / (cmPerRot * 2)) #Calculate how many ticks each wheel needs to turn to rotate the correct amount
        
    if(turnDegrees > 0):
        ser.write(struct.pack('6B', startCommand, turnL, int((wheelSpeed >> 8) & 0xff), int(wheelSpeed & 0xff), ((ticksToRot)), endCommand))
        
    else:
        ser.write(struct.pack('6B', startCommand, turnR, int((wheelSpeed >> 8) & 0xff), int(wheelSpeed & 0xff), ((ticksToRot)), endCommand))
            
    globalPos[2] += turnDegrees
    
#--------------------------------------------------------------------------------------------------------------

def positionEstimation(): #Position estimation step for movement
    for q in range(0, 2):
        ser.write(pollAccel)
        recieveData()
    scaleXAccel()
    scaleYAccel()
    accelXPosEstimate = displacementFromAccelerations(accelerationXScaled[0, 0], accelerationXScaled[1, 0], accelerationXScaled[2, 0], accelerationXScaled[3, 0], accelerationXScaled[0, 1], accelerationXScaled[1, 1], accelerationXScaled[2, 1], accelerationXScaled[3, 1])
    accelYPosEstimate = displacementFromAccelerations(accelerationYScaled[0, 0], accelerationYScaled[1, 0], accelerationYScaled[2, 0], accelerationYScaled[3, 0], accelerationYScaled[0, 1], accelerationYScaled[1, 1], accelerationYScaled[2, 1], accelerationYScaled[3, 1])

    ser.write(pollMove)
    recieveData()
    distQuad = average(leftCmMoved, rightCmMoved)
    distAccel = math.sqrt(accelXPosEstimate ** 2 + accelYPosEstimate ** 2)
    
    distanceTravelled = average(distQuad, distAccel)
    
    radPos = globalPos[2] * (math.pi / 180) #Convert degrees to radians to use with math.trigonometric functions
    globalPos[0] += math.floor((distanceTravelled * math.cos(radPos)) / 2.5)
    globalPos[1] += math.floor((distanceTravelled * math.sin(radPos)) / 2.5) 

#--------------------------------------------------------------------------------------------------------------

#Main while loop of program
while True:
    

    print('\n\n-----------------------------------------------------------------------\n\n1) Current Global Position\n2)Location Of Nearest Obstacle\n3) Move Relative To Local Position\n4) Move To Global Position\n5) Toggle Obstacle Avoidance\n6) Special Functions\n7) Quit\n\nPlease enter your menu choice: ')
    userInput = int(input()) #Print menu and read in user choice
    
    while((userInput > 7) or (userInput < 1)): #Validate user input
        print('\nInvalid input. Please enter a number between 1 and 7\n')
        userInput = int(input())
        
        
        
    
    if (userInput == 1):
    #Current global position

        print('\nCurrent global position: \n')
        print(globalPos)

    #Location of nearest obstacle
    elif (userInput == 2):

        print('\nLocation of nearest obstacle: \n')
        print(objectPos)

    #Move relative to local position
    elif (userInput == 3):

        print('\nEnter x coordinate for new position: ')
        inputX = int(input())
    
        while((inputX > 255) or (inputX < 0)):
            print('\nInvalid input. X coordinate must be a number between 0 and 255\n')
            print('\nEnter x coordinate for new position: ')
            inputX = int(input())
    
        print('\nEnter y coordinate for new position: ')
        inputY = int(input())
    
        while((inputY > 255) or (inputY < 0)):
            print('\nInvalid input. Y coordinate must be a number between 0 and 255\n')
            print('\nEnter y coordinate for new position: ')
            inputY = int(input())
    

        moveLocal(inputX, inputY)

    #Move to global position
    elif (userInput == 4):
        
        print('\nEnter x coordinate for new position: ')
        inputX = int(input())
    
        while((inputX > 255) or (inputX < 0)):
            print('\nInvalid input. X coordinate must be a number between 0 and 255\n')
            print('\nEnter x coordinate for new position: ')
            inputX = int(input())
    
        print('\nEnter y coordinate for new position: ')
        inputY = int(input())
    
        while((inputY > 255) or (inputY < 0)):
            print('\nInvalid input. Y coordinate must be a number between 0 and 255\n')
            print('\nEnter y coordinate for new position: ')
            inputY = int(input())

        moveGlobal(inputX, inputY)

    #Toggle obstacle avoidance
    elif (userInput == 5):

        toggleObst()

    #Special command
    elif (userInput == 6):
        ser.write(specialCommand)


    #Quit
    elif (userInput == 7):

        quitCom = True


    if(quitCom):
        break
