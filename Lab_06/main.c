#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "driverlib/uart.h"
#include "pwm.h"
#include "motor.h"
#include "quadEnc.h"
#include "timer.h"
#include "ir.h"
#include "uart.h"
#include "accel.c"
#include "accel.h"

/**
 * main.c
 */

#define beginCommand 0xAA
#define endCommand 0x55

#define moveForward 0x00
#define moveBack 0x01
#define moveLeft 0x02
#define moveRight 0x03
#define moveBrake 0x04
#define moveFreeStop 0x05

#define sensorBump 0x1F
#define sensorPoll 0x10
#define accelerPoll 0x1f
#define popWheely 0xdd

#define sensor0 0x11
#define sensor1 0x12
#define sensor2 0x13
#define sensor3 0x14
#define sensor4 0x15
#define sensor5 0x16

#define accelerX 0x1e
#define accelerY 0x1d


extern bool bumpDetected;
void bumpCheck()
{
    while( (GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_4) != GPIO_PIN_4) &&
            (GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_5) != GPIO_PIN_5))
        {
          motorForward(true, true, 200, 200);

        }



    if (bumpDetected == true)
    {
        sendData[0] = beginCommand;
        sendData[1] = sensorBump;
        sendData[2] = endCommand;
        bumpDetected = false;
    }
}

void delay()
{
    for( uint32_t i = 0; i < 250000; ++i );
}

void longDelay() //delays for roughly a second
{
    for( uint8_t i = 0; i < 6; ++i )
    {
        delay();
    }
}


int main(void)
{
    pwmInit();
    pwmSetCycle(0, 0);
    pwmEnable();
    motorInit(); //Initialization of required peripherals
    initEncoder();
    initTimer();
    irInit();
    initUART();


    char recieveData[8];
    char sendData[8];
    uint16_t wheelSpeed;
    uint16_t Sense0;
    uint16_t Sense1;
    uint16_t Sense2;
    uint16_t Sense3;
    uint16_t Sense4;
    uint16_t Sense5;
    uint16_t accelX;
    uint16_t accelY;
    uint8_t CmToMove = 0;
    while(1)
        {
            uint8_t index = 0;
            while(UARTCharsAvail(UART1_BASE))
            {
                recieveData[index] = UARTCharGet(UART1_BASE);
                index++;
            }
            index = 0;
            if (recieveData[0] == beginCommand)
            {
                switch(recieveData[1])
                {
                case moveForward:
                    if(recieveData[5] == endCommand)
                    {
                        CmToMove = recieveData[4];
                        wheelSpeed = (recieveData[2] << 8) + recieveData[3];
                        moveXCmForward(cm, wheelSpeed, wheelSpeed);
                    }
                break;

                case moveBack:
                    if(recieveData[5] == endCommand)
                    {
                        CmToMove = recieveData[4];
                        wheelSpeed = (recieveData[2] << 8) + recieveData[3];
                        moveXCmBackward(cm, wheelSpeed, wheelSpeed);
                    }

                break;

                case moveLeft:
                    if(recieveData[5] == endCommand)
                    {
                        CmToMove = recieveData[4];
                        wheelSpeed = (recieveData[2] << 8) + recieveData[3];
                        rotateXCm(true, cm, wheelSpeed, wheelSpeed);
                    }

                break;

                case moveRight:
                    if(recieveData[5] == endCommand)
                    {
                        CmToMove = recieveData[4];
                        wheelSpeed = (recieveData[2] << 8) + recieveData[3];
                        rotateXCm(false, cm, wheelSpeed, wheelSpeed);
                    }

                break;

                case moveBrake:
                    if(recieveData[2] == endCommand)
                    {
                        motorStopBrake(true, true)
                    }

                break;

                case moveFreeStop:
                    if(recieveData[2] == endCommand)
                    {
                        motorStopFree(true, true)
                    }

                break;

                case popWheely:
                    if(recieveData[2] == endCommand)
                    {
                        CmToMove = 4;
                        wheelSpeed = 400;
                        moveXCmForward(cm, wheelSpeed, wheelSpeed);
                    }

                                break;

                case sensorPoll:
                    if(recieveData[2] == endCommand)
                    {
                        Sense0 = getSensorCurrVal_0();

                        sendData[0] = beginCommand;
                        sendData[1] = sensor0;
                        sendData[2] = (Sense0 >> 8) & 0xFF;
                        sendData[3] = Sense0 & 0xFF;
                        sendData[4] = endCommand;
                        sendToUART(sendData);

                        Sense1 = getSensorCurrVal_1();

                        sendData[0] = beginCommand;
                        sendData[1] = sensor1;
                        sendData[2] = (Sense1 >> 8) & 0xFF;
                        sendData[3] = Sense1 & 0xFF;
                        sendData[4] = endCommand;
                        sendToUART(sendData);

                        Sense2 = getSensorCurrVal_2();

                        sendData[0] = beginCommand;
                        sendData[1] = sensor2;
                        sendData[2] = (Sense2 >> 8) & 0xFF;
                        sendData[3] = Sense2 & 0xFF;
                        sendData[4] = endCommand;
                        sendToUART(sendData);

                        Sense3 = getSensorCurrVal_3();

                        sendData[0] = beginCommand;
                        sendData[1] = sensor3;
                        sendData[2] = (Sense3 >> 8) & 0xFF;
                        sendData[3] = Sense3 & 0xFF;
                        sendData[4] = endCommand;
                        sendToUART(sendData);

                        Sense4 = getSensorCurrVal_4();

                        sendData[0] = beginCommand;
                        sendData[1] = sensor4;
                        sendData[2] = (Sense4 >> 8) & 0xFF;
                        sendData[3] = Sense4 & 0xFF;
                        sendData[4] = endCommand;
                        sendToUART(sendData);

                        Sense5 = getSensorCurrVal_5();

                        sendData[0] = beginCommand;
                        sendData[1] = sensor5;
                        sendData[2] = (Sense5 >> 8) & 0xFF;
                        sendData[3] = Sense5 & 0xFF;
                        sendData[4] = endCommand;
                        sendToUART(sendData);
                    }
                break;

                case accelerPoll:
                    if(recieveData[2] == endCommand)
                    {
                        accelX = getSensorCurrVal_X();

                        sendData[0] = beginCommand;
                        sendData[1] = accelerX;
                        sendData[2] = (accelX >> 8) & 0xFF;
                        sendData[3] = accelX & 0xFF;
                        sendData[4] = endCommand;
                        sendToUART(sendData);

                        accelY = getSensorCurrVal_Y();

                        sendData[0] = beginCommand;
                        sendData[1] = accelerY;
                        sendData[2] = (accelY >> 8) & 0xFF;
                        sendData[3] = accelY & 0xFF;
                        sendData[4] = endCommand;
                        sendToUART(sendData);

                    }
                break;

                }
            }



            if (recieveData[0] == beginCommand)
            {
                sendToUART(sendData);
            }


            for(int i = 0; i < 8; i++)
            {
                recieveData[i] = 0x00;
                sendData[i] = 0x00;
            }
        }




}
