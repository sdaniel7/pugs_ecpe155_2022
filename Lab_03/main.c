#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "pwm.h"
#include "motor.h"
#include "quadEnc.h"
#include "timer.h"
#include "ir.h"

/**
 * main.c
 */

void delay()
{
    for( uint32_t i = 0; i < 250000; ++i );
}

void longDelay() //delays for roughly a second
{
    for( uint8_t i = 0; i < 6; ++i )
    {
        delay();
    }
}


int main(void)
{
    pwmInit();
    pwmSetCycle(0, 0);
    pwmEnable();
    motorInit(); //Initialization of required peripherals
    initEncoder();
    initTimer();
    irInit();


    driveXYBox(20, 20);
    while(1){}
}
